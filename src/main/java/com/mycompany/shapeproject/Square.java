/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeproject;

/**
 *
 * @author a
 */
public class Square {
    private double s;

    public Square(double s) {
        this.s = s;
    }

    public double calArea() {
        return s * s;
    }

    public double getS() {
        return s;
    }

    public void setS(double s) {
        if (s <= 0) {
            System.out.println("Error: Square1 must more than zero!!!");
            return;
        }
        this.s = s;
    }
    @Override
    public String toString() {
        return "Area of Square( s = " + this.getS() + ") is " + this.calArea();
    }   
}
