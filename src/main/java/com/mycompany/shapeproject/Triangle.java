/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeproject;

/**
 *
 * @author a
 */
public class Triangle {
    private double b;
    private double h;
    
    public Triangle (double b, double h){
        this.b = b;
        this.h = h;
    }
    public double calArea(){
        return b*h/2;
    }
    public void setInput(double b,double h){
        if( b == 0 || h == 0){
           System.out.println("Base or High is 0 !!!");
           return;
        }
        this.b = b;
        this.h = h;
    }
    public double getB (){
        return b;
    }
    public double getH(){
        return h;
    }
    
    @Override
    public String toString (){
        return "Area of Triangle( Base is "+this.getB()+ " and High is "+this.getH()+" ) = "+this.calArea();
    }
}