/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeproject;

/**
 *
 * @author a
 */
public class Rectangle {
    private double w;
    private double l;
    private int i;

    public Rectangle(double w , double l) {
        this.w = w;
        this.l = l;
    }
    public double calArea() {
        return w * l;
    }
    public void setInput(double w, double l){
        if(w>l){
            System.out.println("Wide should not be equal to long");
            return;
        }if(w==0){
            System.out.println("Wide = 0 !");
        }if(l==0){
            System.out.println("Long = 0 !");
        }if(l==w){
            System.out.println("Wide = Long !");
        }
        this.w = w;
        this.l = l;
    }
    public double getL (){
        return this.l;
        }
    public double getW (){
        return this.w;
        }

    @Override
    public String toString() {
       return "Area of Rectangle(Wide = " + this.getW() + " and Long is " + this.getL() + " ) = " + this.calArea();
    }  
}